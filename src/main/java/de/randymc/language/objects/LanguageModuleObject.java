package de.randymc.language.objects;

import dev.morphia.annotations.Entity;
import lombok.Getter;

import java.util.List;

/*
 * Class created at 00:55 - 11.09.2020
 * Copyright (C) 2020 Shlovto
 */
@Getter
@Entity( value = "language-modules", useDiscriminator = false )
public class LanguageModuleObject {

    private String moduleKey;
    private List<String> paths;
}