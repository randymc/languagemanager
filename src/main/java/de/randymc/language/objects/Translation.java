package de.randymc.language.objects;

import dev.morphia.annotations.Entity;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/*
 * Class created at 11:34 - 10.09.2020
 * Copyright (C) 2020 Shlovto
 */
@Data
@Entity( value = "translations", useDiscriminator = false )
public class Translation {
    private String locale;
    private String key;
    private Map<String, String> values;

    public Translation() {
        this.values = new HashMap<>();
    }

    public Translation( final String locale, final String key, final Map<String, String> values ) {
        this.values = new HashMap<>();
        this.locale = locale;
        this.key = key;
        this.values = values;
    }
}