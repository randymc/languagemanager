package de.randymc.language.objects;

import dev.morphia.annotations.Entity;
import lombok.Getter;

import java.util.Locale;

/*
 * Class created at 19:41 - 09.09.2020
 * Copyright (C) 2020 Shlovto
 */
@Getter
@Entity( value = "languages", useDiscriminator = false )
public class LanguageObject {

    private String locale;
    private String uniqueId;
    private String textureValue;

    public Locale getLocale() {
        return Locale.forLanguageTag( locale );
    }
}