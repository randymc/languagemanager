package de.randymc.language;

import de.randymc.language.loader.FileLanguageLoader;
import de.randymc.language.loader.ILanguageLoader;
import de.randymc.language.loader.MongoLanguageLoader;
import de.randymc.language.objects.LanguageModuleObject;
import de.randymc.language.objects.LanguageObject;
import de.randymc.language.util.LanguageLoaderType;
import dev.morphia.Datastore;
import lombok.Data;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/*
 * Class created at 16:48 - 28.08.2020
 * Copyright (C) 2020 Shlovto
 */
@Data
public class LanguageManager {

    /**
     * Compile command:
     * mvn clean compile -U assembly:single -DskipTests
     */

    private ILanguageLoader languageLoader;
    private Datastore datastore;
    private String storage;
    private HashMap<Locale, LanguageObject> localeHeads;
    private HashMap<String, LanguageModuleObject> prePaths;
    private ArrayList<String> loadedPrePaths;
    private ArrayList<String> paths;

    /**
     * Create a new instance of the LocaleManager
     *
     * @param path defines the Path where the locales are
     * @return returns the new instance
     */
    public static LanguageManager newManager( LanguageLoaderType loaderType, Datastore datastore, String storage, String path ) {
        LanguageManager languageManager = new LanguageManager();
        languageManager.setDatastore( datastore );
        languageManager.setStorage( storage );
        languageManager.setLocaleHeads( new HashMap<>() );
        languageManager.setPaths( new ArrayList<>() );
        languageManager.getPaths().add( path );

        if ( loaderType == LanguageLoaderType.MONGO ) {
            languageManager.setLanguageLoader( new MongoLanguageLoader( languageManager, datastore ) );
        } else {
            languageManager.setLanguageLoader( new FileLanguageLoader( languageManager, datastore ) );
        }

        try {
            languageManager.reloadLocales();
        } catch ( Exception exception ) {
            exception.printStackTrace();
        }
        return languageManager;
    }

    /**
     * Create a new instance of the LocaleManager
     *
     * @param paths defines the Path where the locales are
     * @return returns the new instance
     */
    public static LanguageManager newManager( LanguageLoaderType loaderType, Datastore datastore, String storage, String... paths ) {
        LanguageManager languageManager = new LanguageManager();
        languageManager.setDatastore( datastore );
        languageManager.setStorage( storage );
        languageManager.setLocaleHeads( new HashMap<>() );
        languageManager.setPaths( new ArrayList<>( Arrays.asList( paths ) ) );

        if ( loaderType == LanguageLoaderType.MONGO ) {
            languageManager.setLanguageLoader( new MongoLanguageLoader( languageManager, datastore ) );
        } else {
            languageManager.setLanguageLoader( new FileLanguageLoader( languageManager, datastore ) );
        }

        try {
            languageManager.reloadLocales();
        } catch ( Exception exception ) {
            exception.printStackTrace();
        }
        return languageManager;
    }

    /**
     * Create a new instance of the LocaleManager
     *
     * @param paths defines the Paths where the locales are
     * @return returns the new instance
     */
    public static LanguageManager newManager( LanguageLoaderType loaderType, Datastore datastore, String storage, ArrayList<String> paths ) {
        LanguageManager languageManager = new LanguageManager();
        languageManager.setDatastore( datastore );
        languageManager.setStorage( storage );
        languageManager.setLocaleHeads( new HashMap<>() );
        languageManager.setPaths( paths );

        if ( loaderType == LanguageLoaderType.MONGO ) {
            languageManager.setLanguageLoader( new MongoLanguageLoader( languageManager, datastore ) );
        } else {
            languageManager.setLanguageLoader( new FileLanguageLoader( languageManager, datastore ) );
        }

        try {
            languageManager.reloadLocales();
        } catch ( Exception exception ) {
            exception.printStackTrace();
        }
        return languageManager;
    }

    /**
     * Create a new instance of the LocaleManager
     * This will load the pre-defined paths from the database which can be load by loadPrePath/s
     *
     * @return returns the new instance
     */
    public static LanguageManager newManager( LanguageLoaderType loaderType, Datastore datastore, String storage ) {
        LanguageManager languageManager = new LanguageManager();
        languageManager.setDatastore( datastore );
        languageManager.setStorage( storage );
        languageManager.setLocaleHeads( new HashMap<>() );
        languageManager.setLoadedPrePaths( new ArrayList<>() );
        languageManager.setPrePaths( new HashMap<>() );

        List<LanguageModuleObject> modules = datastore.find( LanguageModuleObject.class ).iterator().toList();
        if ( modules != null && !modules.isEmpty() ) {
            modules.forEach( module -> languageManager.prePaths.put( module.getModuleKey(), module ) );
        }

        if ( loaderType == LanguageLoaderType.MONGO ) {
            languageManager.setLanguageLoader( new MongoLanguageLoader( languageManager, datastore ) );
        } else {
            languageManager.setLanguageLoader( new FileLanguageLoader( languageManager, datastore ) );
        }

        try {
            languageManager.reloadLocales();
        } catch ( Exception exception ) {
            exception.printStackTrace();
        }
        return languageManager;
    }

    /**
     * Add a multi Paths to the locale
     *
     * @param paths defines the Paths where locales are
     */
    public void addPaths( List<String> paths ) {
        if ( this.paths == null ) this.paths = new ArrayList<>();
        paths.forEach( this::addPath );
    }

    /**
     * Add a Path to the locale
     *
     * @param path defines the Path where locales are
     */
    public void addPath( String path ) {
        if ( this.paths == null ) this.paths = new ArrayList<>();
        if ( this.paths.contains( path ) ) return;
        this.paths.add( path );
    }

    /**
     * Add a Path to the locale
     *
     * @param moduleKey defines the module key of the pre path entry
     */
    public void addPrePath( String moduleKey ) {
        if ( this.prePaths == null ) return;
        if ( !this.prePaths.containsKey( moduleKey ) ) return;
        this.addPaths( this.prePaths.get( moduleKey ).getPaths() );
        this.loadedPrePaths.add( moduleKey );
    }

    /**
     * Add a Path to the locale
     *
     * @param moduleKeys defines the module keys of the pre path entries
     */
    public void addPrePaths( String... moduleKeys ) {
        if ( this.prePaths == null ) return;
        for ( String moduleKey : moduleKeys ) {
            this.addPrePath( moduleKey );
            this.loadedPrePaths.add( moduleKey );
        }
    }

    /**
     * Reload all Locales for new Messages etc
     *
     * @throws NullPointerException throws if the path is null
     */
    public void reloadPrePaths() throws Exception {
        if ( this.prePaths == null ) return;
        this.prePaths.clear();

        List<LanguageModuleObject> modules = datastore.find( LanguageModuleObject.class ).iterator().toList();
        if ( modules != null && !modules.isEmpty() ) {
            modules.forEach( module -> prePaths.put( module.getModuleKey(), module ) );
        }

        this.paths.clear();
        addPrePaths( this.loadedPrePaths.toArray( new String[]{} ) );
    }

    /**
     * Reload all Locales for new Messages etc
     *
     * @throws NullPointerException throws if the path is null
     */
    public void reloadLocales() throws Exception {
        localeHeads.clear();

        this.datastore.createQuery( LanguageObject.class ).iterator().toList().forEach( o -> localeHeads.put( o.getLocale(), o ) );

        if ( this.paths == null || this.paths.isEmpty() ) {
            System.err.println( "Language paths are empty or null, is that an error?" );
            return;
        }

        languageLoader.reloadLocales();
    }

    public String getPrivateMessage( Locale locale, String key ) throws Exception {
        return languageLoader.translate( locale, key ).replace( "´", "'" );
    }

    /**
     * Get a message from a Locale
     *
     * @param locale defines the locale from which the message should be get
     * @param key    defines the language-key
     * @return returns the message or null
     */
    public String getPreMessage( Locale locale, String key ) throws Exception {
        return languageLoader.translate( locale, key )
                .replace( "%prefix%", "§b»§7" )
                .replace( "%sb_prefix%", "●§7" )
                .replace( "%debug_prefix%", "§bRMC :: DEBUG §8⟩⟩§7" )
                .replace( "´", "'" );
    }

    /**
     * Get a message from a Locale
     *
     * @param locale defines the locale from which the message should be get
     * @param key    defines the language-key
     * @return returns the message or null
     */
    public String getMessage( Locale locale, String key ) throws Exception {
        return languageLoader.translate( locale, key )
                .replace( "%prefix%", "§b»§7" )
                .replace( "%sb_prefix%", "●§7" )
                .replace( "%friend_prefix%", this.getPreMessage( locale, "friend-prefix" ) )
                .replace( "%party_prefix%", this.getPreMessage( locale, "party-prefix" ) )
                .replace( "%achievement_prefix%", this.getPreMessage( locale, "achievement-prefix" ) )
                .replace( "%debug_prefix%", "§bRMC :: DEBUG §8⟩⟩§7" )
                .replace( "´", "'" );
    }

    /**
     * Get a message from a Locale
     *
     * @param locale defines the locale from which the message should be get
     * @param key    defines the translation-key
     * @return returns the message or null
     */
    public String getMessage( Locale locale, String key, Object... objects ) throws Exception {
        List<Object> objectsList = new ArrayList<>();
        for ( Object object : objects ) {
            if ( !( object instanceof String ) ) {
                objectsList.add( object );
                continue;
            }

            String string = (String) object;
            if ( string.startsWith( "translate;" ) ) {
                objectsList.add( getMessage( locale, string.split( ";" )[1] ) );
            }
        }
        return MessageFormat.format( languageLoader.translate( locale, key )
                .replace( "%prefix%", "§b»§7" )
                .replace( "%sb_prefix%", "●§7" )
                .replace( "%friend_prefix%", this.getPreMessage( locale, "friend-prefix" ) )
                .replace( "%party_prefix%", this.getPreMessage( locale, "party-prefix" ) )
                .replace( "%achievement_prefix%", this.getPreMessage( locale, "achievement-prefix" ) )
                .replace( "%debug_prefix%", "§bRMC :: DEBUG §8⟩⟩§7" ), objects )
                .replace( "´", "'" );
    }
}