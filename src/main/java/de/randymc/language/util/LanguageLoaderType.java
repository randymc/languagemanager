package de.randymc.language.util;

/*
 * Enum created at 11:29 - 10.09.2020
 * Copyright (C) 2020 Shlovto
 */
public enum LanguageLoaderType {
    FILE, MONGO
}