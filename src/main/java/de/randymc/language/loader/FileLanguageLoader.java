package de.randymc.language.loader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.randymc.language.LanguageManager;
import de.randymc.language.configuration.LanguageConfiguration;
import dev.morphia.Datastore;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/*
 * Class created at 11:27 - 10.09.2020
 * Copyright (C) 2020 Shlovto
 */
public class FileLanguageLoader implements ILanguageLoader {

    private final LanguageManager languageManager;
    private final Datastore datastore;
    private final Map<String, JSONObject> singleTranslations;
    private final Map<Locale, JSONObject> locales;

    public FileLanguageLoader( LanguageManager languageManager, Datastore datastore ) {
        this.languageManager = languageManager;
        this.datastore = datastore;
        this.singleTranslations = new HashMap<>();
        this.locales = new HashMap<>();
    }

    @Override
    public void reloadLocales() throws NullPointerException {
        singleTranslations.clear();
        locales.clear();

        for ( Locale locale : Locale.getAvailableLocales() ) {
            File localeFolder = new File( languageManager.getStorage() + "/" + locale.toLanguageTag() + "/" );
            if ( !localeFolder.exists() ) continue;
            if ( !localeFolder.canRead() ) {
                System.out.println( "Can't read '" + localeFolder.toString() + "'" );
                continue;
            }

            JSONObject jsonObject = new JSONObject();

            for ( String path : languageManager.getPaths() ) {
                String language = path.split( "/" )[path.split( "/" ).length - 1];
                File file = new File( languageManager.getStorage() + "/" + locale.toLanguageTag() + "/" + path + "/" + language + ".json" );
                if ( !file.exists() ) {
                    System.out.println( "'" + file.toString() + "' does not exist." );
                    continue;
                }
                if ( !file.canRead() ) {
                    System.out.println( "Can't read '" + file.toString() + "'" );
                    continue;
                }
                if ( !file.exists() && !file.canRead() ) continue;
                if ( file.exists() ) {
                    LanguageConfiguration languageConfiguration = new LanguageConfiguration( languageManager.getStorage() + "/" + locale.toLanguageTag() + "/" + path + "/" + language + ".json" );
                    languageConfiguration.readConfiguration();

                    singleTranslations.put( path, languageConfiguration.getJsonObject() );

                    for ( String string : languageConfiguration.getJsonObject().keySet() ) {
                        jsonObject.put( string, languageConfiguration.getJsonObject().get( string ) );
                    }
                }
            }

            if ( !jsonObject.keySet().isEmpty() ) {
                locales.put( locale, jsonObject );
            }
        }
    }

    @Override
    public ILanguageLoader convertTo( ILanguageLoader languageLoader ) throws NullPointerException {
        return languageLoader.convertFrom( this );
    }

    @Override
    public ILanguageLoader convertFrom( ILanguageLoader languageLoader ) throws NullPointerException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        System.out.println( "Different language loader has " + languageLoader.getSingleTranslation().size() + " single messages loaded" );
        System.out.println( "Different language loader has " + languageLoader.getLocales().size() + " messages loaded" );
        System.out.println( "Language Manager has " + languageManager.getPaths().size() + " paths loaded" );

        for ( Locale locale : languageLoader.getLocales().keySet() ) {
            for ( String path : languageManager.getPaths() ) {
                File file = new File( languageManager.getStorage() + "/" + locale.toLanguageTag() + "/" + path );
                file.mkdirs();

                String language = path.split( "/" )[path.split( "/" ).length - 1];

                try ( FileWriter fileWriter = new FileWriter( new File( languageManager.getStorage() + "/" + locale.toLanguageTag() + "/" + path + "/" + language + ".json" ) ) ) {
                    gson.toJson( languageLoader.getSingleTranslation().get( path ), fileWriter );
                    System.out.println( "Successfully write translation to \"" + languageManager.getStorage() + "/" + locale.toLanguageTag() + "/" + path + "/" + language + ".json" + "\"" );
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }

        this.reloadLocales();

        return this;
    }

    @Override
    public String translate( Locale locale, String key ) throws NullPointerException {
        if ( !locales.containsKey( locale ) ) {
            System.err.println( "The locale \"" + locale.toLanguageTag() + "\" was not found." );
            if ( locale != Locale.GERMANY ) {
                return this.translate( Locale.GERMANY, key );
            } else {
                return "N/A";
            }
        }
        if ( !locales.get( locale ).has( key ) ) {
            System.err.println( "The message key '" + key + "' was not found or is invalid." );
            if ( locale != Locale.GERMANY ) {
                return this.translate( Locale.GERMANY, key );
            } else {
                return "N/A";
            }
        }
        return locales.get( locale ).getString( key );
    }

    @Override
    public Map<String, JSONObject> getSingleTranslation() {
        return singleTranslations;
    }

    @Override
    public Map<Locale, JSONObject> getLocales() throws NullPointerException {
        return locales;
    }
}