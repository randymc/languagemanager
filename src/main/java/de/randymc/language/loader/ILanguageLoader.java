package de.randymc.language.loader;

import org.json.JSONObject;

import java.util.Locale;
import java.util.Map;

/*
 * Interface created at 11:24 - 10.09.2020
 * Copyright (C) 2020 Shlovto
 */
public interface ILanguageLoader {

    /**
     * Reload all Locales for new Messages etc
     *
     * @throws NullPointerException throws if any errors occur
     */
    public void reloadLocales() throws NullPointerException;

    /**
     * Convert a language to other one
     *
     * @throws NullPointerException throws if any errors occur
     */
    public ILanguageLoader convertTo( ILanguageLoader languageLoader ) throws NullPointerException;

    /**
     * Convert a language from other one
     *
     * @throws NullPointerException throws if any errors occur
     */
    public ILanguageLoader convertFrom( ILanguageLoader languageLoader ) throws NullPointerException;

    /**
     * Returns a message from a specific translation-key
     *
     * @return If the key is invalid or not found 'N/A' or the translated message
     * @throws NullPointerException throws if any errors occur
     */
    public String translate( Locale locale, String key ) throws NullPointerException;

    /**
     * Returns all translations and locales from a specific key
     *
     * @return If the map is invalid null or the translations and locales
     * @throws NullPointerException throws if any errors occur
     */
    public Map<String, JSONObject> getSingleTranslation();

    /**
     * Returns all translations and locales
     *
     * @return If the map is invalid null or the translations and locales
     * @throws NullPointerException throws if any errors occur
     */
    public Map<Locale, JSONObject> getLocales();
}