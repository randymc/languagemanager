package de.randymc.language.loader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.randymc.language.LanguageManager;
import de.randymc.language.objects.Translation;
import dev.morphia.Datastore;
import dev.morphia.query.experimental.filters.Filters;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/*
 * Class created at 11:43 - 10.09.2020
 * Copyright (C) 2020 Shlovto
 */
public class MongoLanguageLoader implements ILanguageLoader {

    private final LanguageManager languageManager;
    private final Datastore datastore;
    private final Map<String, JSONObject> singleTranslations;
    private final Map<Locale, JSONObject> locales;

    public MongoLanguageLoader( LanguageManager languageManager, Datastore datastore ) {
        this.languageManager = languageManager;
        this.datastore = datastore;
        this.singleTranslations = new HashMap<>();
        this.locales = new HashMap<>();
    }

    @Override
    public void reloadLocales() throws NullPointerException {
        singleTranslations.clear();
        locales.clear();

        for ( Locale locale : Locale.getAvailableLocales() ) {
            if ( this.datastore.find( Translation.class ).filter( Filters.eq( "locale", locale.toLanguageTag() ) ).first() != null ) {
                Map<String, String> values = new HashMap<>();

                for ( String path : languageManager.getPaths() ) {
                    Translation translation = this.datastore.find( Translation.class ).filter( Filters.eq( "locale", locale.toLanguageTag() ), Filters.eq( "key", path ) ).first();
                    if ( translation != null ) {
                        values.putAll( translation.getValues() );
                        singleTranslations.put( path, new JSONObject( translation.getValues() ) );
                    }
                }

                JSONObject jsonObject = new JSONObject( values );
                this.locales.put( locale, jsonObject );
            }
        }
    }

    @Override
    public ILanguageLoader convertTo( ILanguageLoader languageLoader ) throws NullPointerException {
        return languageLoader.convertFrom( this );
    }

    @Override
    public ILanguageLoader convertFrom( ILanguageLoader languageLoader ) throws NullPointerException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        System.out.println( "Different language loader has " + languageLoader.getSingleTranslation().size() + " single messages loaded" );
        System.out.println( "Different language loader has " + languageLoader.getLocales().size() + " messages loaded" );
        System.out.println( "Language Manager has " + languageManager.getPaths().size() + " paths loaded" );

        for ( Locale locale : languageLoader.getLocales().keySet() ) {
            for ( String path : languageManager.getPaths() ) {
                Translation translation = new Translation();
                translation.setLocale( locale.toLanguageTag() );
                translation.setKey( path );
                translation.setValues( gson.fromJson( languageLoader.getSingleTranslation().get( path ).toString(), HashMap.class ) );

                this.datastore.save( translation );
                System.out.println( "Successfully upload translation \"" + path + "\" to MongoDB" );
            }
        }

        this.reloadLocales();

        return this;
    }

    @Override
    public String translate( Locale locale, String key ) throws NullPointerException {
        if ( !locales.containsKey( locale ) ) {
            System.err.println( "The locale \"" + locale.toLanguageTag() + "\" was not found." );
            if ( locale != Locale.GERMANY ) {
                return this.translate( Locale.GERMANY, key );
            } else {
                return "N/A";
            }
        }
        if ( !locales.get( locale ).has( key ) ) {
            System.err.println( "The message key '" + key + "' was not found or is invalid." );
            if ( locale != Locale.GERMANY ) {
                return this.translate( Locale.GERMANY, key );
            } else {
                return "N/A";
            }
        }
        return locales.get( locale ).getString( key );
    }

    @Override
    public Map<String, JSONObject> getSingleTranslation() {
        return singleTranslations;
    }

    @Override
    public Map<Locale, JSONObject> getLocales() throws NullPointerException {
        return locales;
    }
}