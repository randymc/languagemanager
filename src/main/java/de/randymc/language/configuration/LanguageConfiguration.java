package de.randymc.language.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.Getter;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/*
 * Class created at 16:51 - 28.08.2020
 * Copyright (C) 2020 Shlovto
 */
public class LanguageConfiguration {
    private final File configFile;
    @Getter( AccessLevel.PUBLIC )
    private JSONObject jsonObject;
    private final Gson gson;

    /**
     * Create a new Locale Configuration Instance
     *
     * @param path defines the path and the configuration file
     */
    public LanguageConfiguration( String path ) {
        this.configFile = new File( path );
        this.gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();
    }

    /**
     * Check if the Config File exist
     *
     * @return returns true if the file exist and false if not
     */
    public boolean exists() {
        return configFile.exists();
    }

    /**
     * Read the Config File and parse it to a Json Object
     */
    public void readConfiguration() {
        try ( BufferedReader fileReader = new BufferedReader( new InputStreamReader( new FileInputStream( configFile ), StandardCharsets.UTF_8 ) ) ) {
            JsonObject json = gson.fromJson( fileReader, JsonObject.class );
            jsonObject = new JSONObject( json.toString().replace( "'", "´" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}